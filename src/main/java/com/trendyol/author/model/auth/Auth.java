package com.trendyol.author.model.auth;

import lombok.*;

import java.util.UUID;

@Getter
@Setter

public class Auth {

    private String id;
    private String userName;
    private String email;
    private String password;
    private String token;

    public Auth(String userName, String email, String password) {
        this.id= UUID.randomUUID().toString();
        this.userName = userName;
        this.email = email;
        this.password = password;
    }

    public Auth() {
        this.id= UUID.randomUUID().toString();
    }

}
