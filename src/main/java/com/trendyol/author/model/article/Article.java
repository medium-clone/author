package com.trendyol.author.model.article;

import com.trendyol.author.common.enums.PublishStatus;
import com.trendyol.author.model.article.statistic.Statistic;
import com.trendyol.author.model.interest.Interest;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Article {
    private String id;
    private List<String> authorId;
    private String title;
    private Content content;
    private String featuredImageUrl;
    private List<Interest> tags;
    private PublishStatus publishStatus;
    private Long publishDate;
    private Statistic statistic;
}
