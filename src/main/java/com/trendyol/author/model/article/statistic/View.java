package com.trendyol.author.model.article.statistic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class View {
    private Long dateTime;
    private String authorId;
}
