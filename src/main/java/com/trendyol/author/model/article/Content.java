package com.trendyol.author.model.article;

import com.trendyol.author.common.enums.ArticleType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class Content {
    private String id;
    private ArticleType articleType;
    private String url;
}
