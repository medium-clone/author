package com.trendyol.author.model.article.statistic;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Statistic {
    private String articleId;
    private List<View> views;
    private List<Read> reads;
    private int viewCount;
    private int readCount;
    private int averageRead;
}
