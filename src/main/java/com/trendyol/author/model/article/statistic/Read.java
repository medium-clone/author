package com.trendyol.author.model.article.statistic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Read {
    private String authorId;
    private Long startDateTime;
    private Long exitDateTime;
}
