package com.trendyol.author.model.author;


import com.trendyol.author.model.article.Article;
import com.trendyol.author.model.interest.Interest;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Author {
    private String id;
    private About about;
    private List<Article> articles;
    private List<Interest> interests;
    private List<Follow> followers;
    private List<Follow> followings;
}

