package com.trendyol.author.model.author;

import lombok.Data;

@Data
public class SocialMedia {
    private String id;
    private String twitter;
    private String website;
    private String github;
    private String linkedin;
}
