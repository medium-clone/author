package com.trendyol.author.model.author;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class About {
    private String id;
    private String name;
    private String profileUrl;
    private String profilePhotoUrl;
    private SocialMedia socialMedia;
}
