package com.trendyol.author.service;


import com.trendyol.author.repository.AuthorRepository;
import org.springframework.stereotype.Service;

@Service
public class ArticleService {

    private AuthorRepository authorRepository;

    public ArticleService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }
}
