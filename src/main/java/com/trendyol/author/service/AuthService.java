package com.trendyol.author.service;

import com.trendyol.author.model.auth.Auth;
import com.trendyol.author.repository.AuthRepository;

import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private final AuthRepository authRepository;
    //  private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public AuthService(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    public Auth createAuth(Auth auth) {
        auth.setPassword(encodePassword(auth.getPassword()));
        auth.setToken(createToken(auth.getId(), auth.getUserName()));
        authRepository.create(auth);
        return auth;
    }

    private String encodePassword(String password) {
        return password;
       // return bCryptPasswordEncoder.encode(password);
    }

    private String createToken(String id, String userName) {
        //create token
        return id + "?" + userName;
    }

}
