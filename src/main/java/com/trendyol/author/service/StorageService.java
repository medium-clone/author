package com.trendyol.author.service;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class StorageService {


    public String uploadImage(MultipartFile file,String fileContentType) {
        final String uri = "http://localhost:8081/storage/";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = null;
        try {
            response = restTemplate
                    .postForEntity(uri, file.getBytes(), String.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert response != null;
        return response.getBody();
    }

}
