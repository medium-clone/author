package com.trendyol.author.service;

import com.trendyol.author.repository.InterestRepository;
import org.springframework.stereotype.Service;

@Service
public class InterestService {

    private InterestRepository interestRepository;

    public InterestService(InterestRepository interestRepository) {
        this.interestRepository = interestRepository;
    }
}
