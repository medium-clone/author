package com.trendyol.author.common.enums;

public enum PublishStatus {
    UNLISTED,PUBLIC,DRAFT,PRIVATE
}