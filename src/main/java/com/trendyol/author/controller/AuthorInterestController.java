package com.trendyol.author.controller;

import com.trendyol.author.model.interest.Interest;
import com.trendyol.author.service.AuthService;
import com.trendyol.author.service.AuthorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/author/{id}/interest")
public class AuthorInterestController {

    private AuthService authService;
    private AuthorService authorService;

    public AuthorInterestController(AuthService authService, AuthorService authorService) {
        this.authService = authService;
        this.authorService = authorService;
    }

    @GetMapping
    public ResponseEntity<List<Interest>> getAuthorInterest(@PathVariable String id){

        // get the interest with interest service
        // return interest list

        return ResponseEntity.ok().build();
    }

    @PostMapping
    public ResponseEntity<String> addInterest(@RequestBody Interest interest, @PathVariable String id, @RequestHeader String token){

        // control token
        // check user with auth service
        // add interest to author interest list

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{interest-id}")
    public ResponseEntity<String> deleteInterest(@PathVariable String id, @PathVariable(value = "interest-id") String interestId, @RequestHeader String token){

        // control token
        // check user with auth service
        // delete interest from author interest list

        return ResponseEntity.ok().build();
    }
}
