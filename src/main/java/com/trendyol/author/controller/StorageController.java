package com.trendyol.author.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.trendyol.author.service.ArticleService;
import com.trendyol.author.service.AuthService;
import com.trendyol.author.service.AuthorService;
import com.trendyol.author.service.StorageService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;

@RestController
@RequestMapping("/storage/{id}")
public class StorageController {

    private AuthService authService;
    private AuthorService authorService;
    private ArticleService articleService;
    private StorageService storageService;

    public StorageController(AuthService authService,
                             AuthorService authorService,
                             ArticleService articleService,
                             StorageService storageService) {
        this.authService = authService;
        this.authorService = authorService;
        this.articleService = articleService;
        this.storageService = storageService;
    }

    @PostMapping("/profile")
    public ResponseEntity<String> uploadProfilePhoto(MultipartFile file, @RequestHeader String token,
                                                     @PathVariable String id) {


        // token and id will check with auth service

        // storage service upload image
        // storage service return upload image url
        String url = storageService.uploadImage(file, file.getContentType());

        // update author profile photo url with return url author service

        return ResponseEntity.ok(url);
    }

    @PostMapping("/article/{article-id}/featured-image")
    public ResponseEntity<URI> uploadArticleFeaturedImage(MultipartFile file, @RequestHeader String token,
                                                          @PathVariable String id,
                                                          @PathVariable(value = "article-id") String articleId) {

        // check token and id with auth service
        // upload featured image with storage service
        // return featured image url with storage service
        // update article featured image with return url with article service

        return ResponseEntity.ok().build();
    }

    @PostMapping("/article/{article-id}/content")
    public ResponseEntity<URI> uploadArticleContentImage(MultipartFile file, @RequestHeader String token,
                                                         @PathVariable String id,
                                                         @PathVariable(value = "article-id") String articleId) {

        // check token and id with auth service
        // upload image with storage service
        // return url

        return ResponseEntity.ok().build();
    }

}
