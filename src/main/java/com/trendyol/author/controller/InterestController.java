package com.trendyol.author.controller;

import com.trendyol.author.model.interest.Interest;
import com.trendyol.author.service.InterestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/interest")
public class InterestController {

    private InterestService interestService;

    public InterestController(InterestService interestService) {
        this.interestService = interestService;
    }

    @GetMapping
    public ResponseEntity<List<Interest>> getInterest(){
        // get interests from interest service
        // return interest list

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<Interest>> getInterest(@PathVariable String id){
        // get interest from interest service
        // return interest

        return ResponseEntity.ok().build();
    }

    @PostMapping
    public ResponseEntity<URI> addInterest(@RequestBody Interest interest){

        // add interest with interest service

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteInterest(@PathVariable String id){
        // delete interest with interest service

        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{id}")
    public ResponseEntity<URI> updateInterest(@PathVariable String id){

        //update interest with interest service

        return ResponseEntity.ok().build();
    }

}
