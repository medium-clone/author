package com.trendyol.author.controller;

import com.trendyol.author.common.Queries;
import com.trendyol.author.model.article.statistic.Statistic;
import com.trendyol.author.service.ArticleService;
import com.trendyol.author.service.AuthService;
import com.trendyol.author.service.StatisticService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/author/{id}/statistic")
public class StatisticController {

    private AuthService authService;
    private ArticleService articleService;
    private StatisticService statisticService;

    public StatisticController(AuthService authService,
                               ArticleService articleService,
                               StatisticService statisticService) {
        this.authService = authService;
        this.articleService = articleService;
        this.statisticService = statisticService;
    }



    @GetMapping
    public ResponseEntity<List<Statistic>> getStatistics(@RequestHeader String token, @PathVariable String id) {

        // check token and id auth service
        // get all article list with article service
        // send article list to statistic service
        // return articles statistics

        return ResponseEntity.ok().build();
    }

    @GetMapping("/article/{article-id}")
    public ResponseEntity<Statistic> getStatistic(@RequestHeader String token, @PathVariable String id,
                                                  @PathVariable(value = "article-id") String articleId) {

        // check token and id auth service
        // return article to statistic service
        // return articles statistics

        return ResponseEntity.ok().build();
    }
}
