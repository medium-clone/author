package com.trendyol.author.controller;

import com.trendyol.author.model.author.Author;
import com.trendyol.author.service.AuthService;
import com.trendyol.author.service.AuthorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;


@RestController
@RequestMapping("/author")
public class AuthorController {

    private AuthService authService;
    private AuthorService authorService;

    public AuthorController(AuthService authService, AuthorService authorService) {
        this.authService = authService;
        this.authorService = authorService;
    }

    @GetMapping("/{user-name}")
    public ResponseEntity<Author> getAuthor(@PathVariable(value = "user-name") String userName) {
        // get user with author service
        // return author

        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{user-name}")
    public ResponseEntity<URI> patchAuthor(@PathVariable(value = "user-name") String userName, @RequestBody Author author, @RequestHeader String token) {
        // control token
        // check username with token
        // update author with author service
        // return author url

        return ResponseEntity.ok().build();
    }
}
