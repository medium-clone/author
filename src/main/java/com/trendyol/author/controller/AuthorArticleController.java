package com.trendyol.author.controller;

import com.trendyol.author.model.article.Article;
import com.trendyol.author.service.ArticleService;
import com.trendyol.author.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/author/{id}/article")
public class AuthorArticleController {

    private AuthService authService;
    private  ArticleService articleService;

    public AuthorArticleController(AuthService authService, ArticleService articleService) {
        this.authService = authService;
        this.articleService = articleService;
    }

    @GetMapping
    public ResponseEntity<List<Article>> getAuthorArticles(@PathVariable String id){

        // get author article list with article service
        // return article list

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{article-id}")
    public ResponseEntity<Article> getAuthorArticle(@PathVariable String id,
                                                    @PathVariable(value = "article-id") String articleId){

        // get author article with article service
        // return article

        return ResponseEntity.ok().build();
    }

    @PostMapping
    public ResponseEntity<URI> addArticle(@RequestBody Article article,
                                          @PathVariable String id,
                                          @RequestHeader String token){

        // check token
        // check user with auth service
        // add article to author with article service

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{article-id}")
    public ResponseEntity<Void> deleteArticle(@PathVariable String id,
                                             @PathVariable(value = "article-id") String articleId,
                                             @RequestHeader String token){

        // check token
        // check user with auth service
        // delete author article with article service
        // return article

        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{article-id}")
    public ResponseEntity<URI> updateArticle(@RequestBody Article article,
                                             @PathVariable String id,
                                             @PathVariable(value = "article-id") String articleId,
                                             @RequestHeader String token){
        // check token
        // check user with auth service
        // update author article with article service
        // return article

        return ResponseEntity.ok().build();
    }
}
