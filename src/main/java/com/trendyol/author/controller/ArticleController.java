package com.trendyol.author.controller;

import com.trendyol.author.model.article.Article;
import com.trendyol.author.service.ArticleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticleController {

    private ArticleService articleService;

    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping
    public ResponseEntity<List<Article>> getArticles(@RequestParam(required = false) String tag,
                                                     @RequestParam(required = false) String title,
                                                     @RequestParam(required = false) String userName,
                                                     @RequestParam(required = false) Long publishedDate){

        // get articles with article service
        // return articles

        return ResponseEntity.ok().build();
    }
}
