package com.trendyol.author.controller;

import com.trendyol.author.model.auth.Auth;
import com.trendyol.author.service.AuthService;
import com.trendyol.author.service.AuthorService;

import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;



import java.net.URI;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private AuthService authService;
    private AuthorService authorService;


    public AuthController(AuthService authService, AuthorService authorService) {
        this.authService = authService;
        this.authorService = authorService;
    }

    @PostMapping
    public ResponseEntity<URI> signUp(@RequestBody Auth auth) {

        Auth newAuth= authService.createAuth(auth);
        //create auth with auth service
        //create author with author service
        //create new token with auth service
        //return token and new author url

        return ResponseEntity.ok().build();
    }



    @GetMapping
    public ResponseEntity<Void> signIn(@RequestParam String email, @RequestParam String password) {

        //read email and password with auth service
        //create new token with auth service
        //return token

        return ResponseEntity.ok().build();
    }

    @PatchMapping
    public ResponseEntity<URI> updateUser(@RequestBody Auth auth, @RequestHeader String token) {

        //control token with auth service
        //update auth with auth service
        //create new token with auth service
        //return token

        return ResponseEntity.ok().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteUser(@RequestParam String email, @RequestParam String password, @RequestHeader String token) {

        //control token with auth service
        //delete author with author service
        //delete auth with auth service

        return ResponseEntity.ok().build();
    }

}
