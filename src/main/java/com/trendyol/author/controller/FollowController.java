package com.trendyol.author.controller;

import com.trendyol.author.model.author.Follow;
import com.trendyol.author.service.AuthService;
import com.trendyol.author.service.AuthorService;
import com.trendyol.author.service.FollowService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/author/{user-name}")
public class FollowController {

    private AuthService authService;
    private AuthorService authorService;
    private FollowService followService;

    public FollowController(AuthService authService,
                            AuthorService authorService,
                            FollowService followService) {
        this.authService = authService;
        this.authorService = authorService;
        this.followService = followService;
    }

    @GetMapping("/followers")
    public ResponseEntity<List<Follow>> getAuthorsFollowers(@PathVariable(value = "user-name")
                                                                        String userName) {
        // get followers with author service
        // return follower list

        return ResponseEntity.ok().build();
    }

    @GetMapping("/followings")
    public ResponseEntity<List<Follow>> getAuthorsFollowings(@PathVariable(value = "user-name") String userName) {
        // get followings with author service
        // return followings list

        return ResponseEntity.ok().build();
    }

    @PostMapping("/followings/{follow-user-name}")
    public ResponseEntity<URI> followAuthor(@PathVariable(value = "user-name") String userName,
                                            @PathVariable(value = "follow-user-name") String followUserName,
                                            @RequestHeader String token) {
        // control token
        // check user with token
        // check follow user name
        // add author to followingList with follower service
        // follow user name add to author followerList

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/followings/{follower-user-name}")
    public ResponseEntity<Void> unfollowAuthor(@PathVariable(value = "user-name") String userName,
                                             @PathVariable(value = "follower-user-name") String followUserName,
                                             @RequestHeader String token) {
        // control token
        // check user with token
        // check follow user name
        // delete author from followingList with follower service
        // delete follower user name from author followerList

        return ResponseEntity.ok().build();
    }
}
