package com.trendyol.author.repository;


import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.trendyol.author.common.Queries;
import org.springframework.stereotype.Repository;


@Repository
public class InterestRepository {

    private Cluster couchbaseCluster;
    private Collection interestCollection;

    public InterestRepository(Cluster couchbaseCluster, Collection interestCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.interestCollection = interestCollection;
    }
}
