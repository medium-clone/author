package com.trendyol.author.repository;


import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorRepository {


    private Cluster couchbaseCluster;
    private Collection  authorCollection;

    public AuthorRepository(Cluster couchbaseCluster, Collection authorCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.authorCollection = authorCollection;
    }

}
