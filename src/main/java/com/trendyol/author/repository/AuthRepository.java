package com.trendyol.author.repository;


import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.trendyol.author.model.auth.Auth;
import org.springframework.stereotype.Repository;

@Repository
public class AuthRepository {

    private Cluster couchbaseCluster;
    private Collection authCollection;

    public AuthRepository(Cluster couchbaseCluster, Collection authCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.authCollection = authCollection;
    }

    public void create(Auth auth) {
        authCollection.insert(auth.getId(), auth);
    }
}
